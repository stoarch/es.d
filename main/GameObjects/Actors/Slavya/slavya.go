components {
  id: "script"
  component: "/main/scripts/actors/slavya/slavya_actor_manager.script"
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "blink_happy_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Actors/Slavya/Anims/slavya_blink.atlas\"\n"
  "default_animation: \"pose2_blink_anim\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 29.806
    y: 180.77728
    z: 0.3
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "blink_normal_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Actors/Slavya/Anims/Pose1/Normal/Blink/slavya_blink.tilesource\"\n"
  "default_animation: \"normal\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -16.21413
    y: 177.28456
    z: 0.3
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "body"
  type: "sprite"
  data: "tile_set: \"/main/images/Actors/Slavya/slavya_bodies.atlas\"\n"
  "default_animation: \"sl_1_body\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.05
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "emotion"
  type: "sprite"
  data: "tile_set: \"/main/images/Actors/Slavya/slavya_emotions.atlas\"\n"
  "default_animation: \"sl_1_normal\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.2
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "outfit"
  type: "sprite"
  data: "tile_set: \"/main/images/Actors/Slavya/slavya_outfits.atlas\"\n"
  "default_animation: \"sl_1_pioneer\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.1
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
