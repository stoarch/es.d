embedded_components {
  id: "actor_icon_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Actors/Slavya/SmallIcons/slavya_small_icons.atlas\"\n"
  "default_animation: \"slavya_smile_pioneer_close\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: 0.0
    y: 0.0
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart10_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -179.79723
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart1_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"Heart-icon\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -1444.9686
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart2_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -1303.4696
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart3_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -1164.5634
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart4_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -1024.9445
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart5_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -888.73083
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart6_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -736.0351
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart7_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -594.53595
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart8_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -455.6298
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
embedded_components {
  id: "heart9_sprite"
  type: "sprite"
  data: "tile_set: \"/main/images/Gui/Icons/gui_icons.atlas\"\n"
  "default_animation: \"black_heart_icon_128x128\"\n"
  "material: \"/builtins/materials/sprite.material\"\n"
  "blend_mode: BLEND_MODE_ALPHA\n"
  ""
  position {
    x: -316.01083
    y: -10.075202
    z: 0.0
  }
  rotation {
    x: 0.0
    y: 0.0
    z: 0.0
    w: 1.0
  }
}
