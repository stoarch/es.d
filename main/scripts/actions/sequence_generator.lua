local M = {}

function M.make_sequence( action, ... )
-- make function, that call all params in sequence
	local args = {...}
	local laction = action
	
	return 
		function()
			if( laction ~= nil )then
				laction()
			end

			
			if( args ~= nil )then
				for id, seq in ipairs( args ) do
					if( seq ~= nil )then
						seq()
					end
				end
			end
		end
end 

return M