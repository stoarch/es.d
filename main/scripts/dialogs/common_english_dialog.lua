return {
	["Судя по всему, этот лагерь (если это, конечно, лагерь) – единственое место, где могли быть люди, поэтому я решил пойти туда и уже почти дошёл до ворот, как..."] = "This camp (if, of course, it is really a camp) looked like the only place where people could be, so I decided to go there, and hardly had I reached the gates when...",
	["Оттуда выглянула девочка..."] = "A girl came out from behind them...",
	["... в пионерской форме."] = "... wearing a pioneer uniform.",
	["Логика меня не подвела."] = "My logic didn’t let me down this time.",
	["Впрочем, пионерская форма в XXI веке…"] = "Then again, a pioneer uniform in the 21st century...",	
	["Впрочем, девочки {i}здесь{/i}..."] = "And then again, a girl here...",
	["Я застыл на месте не в силах сделать ни шагу."] = "I froze, unable to take a step.",
	["Очень хотелось убежать."] = "I felt very much like running away. ",
	["Подальше отсюда, подальше от этого автобуса, ворот, статуй, подальше от этой чёртовой птицы с её сиестой."] = "Running as far away as I could from this place, far from this bus, gates, statues and far from this bloody bird with its siesta.",
	["Просто бежать, бежать свободно, словно ветер, бежать всё быстрее, маша рукой пролетающим мимо планетам, подмигивая галактикам."] = "Just running, free like the wind, faster and faster, waving to the planets passing by, winking at the galaxies.",
	["Бежать, став лучом пульсара, превратившись в реликтовое излучение, бежать навстречу неизвестности."] = "Running, becoming a pulsar ray, and turning into vestigial radiation, running to face the unknown.",
	["Бежать куда угодно, но {b}подальше{/b} отсюда!"] = "Run no matter where, as long as it is far away from this place!",
	["Тем временем девочка подошла ближе и улыбнулась."] = "Meanwhile, the girl came closer and smiled.",
	["Её красоту я не смог не отметить, даже трясясь от страха."] = "I could not help but notice her beauty, even though I was trembling with fear.",
	["Человеческие инстинкты работают вопреки сознанию, и если только 5%% мозга отвечают за мыслительную деятельность, то остальные 95 всегда заняты поддержанием {i}жизне{/i}деятельности, в том числе и обеспечивают стабильное функционирование гормональной системы."] = "Human instincts work independant of consciousness, and while only 5%% of the brain is responsible for cognitive processes, the remaining 95%% are always busy sustaining {i}life{/i}, and in particular, ensuring stable functioning of the hormonal system.",
	["Мне так мучительно хотелось быть проще, не думать протокольными фразами из Большой советской энциклопедии, но мысли появлялись в мозгу одна за одной, глупые, неуместные, словно выдернутые из внутреннего монолога героя дрянного детективчика в мягком переплёте."] = "I desperately wanted to get less complicated, and stop thinking in formal quotes from an encyclopedia, though my thoughts appeared one by one, being stupid, out of place, as if taken from an internal monologue of the hero of some junky softcover crime fiction book.",
		
	["Приятное славянское лицо, длинные косы, похожие на две толстые охапки свежескошенного сена, и голубые глаза, в которых, казалось, можно утонуть."] = "A pretty slavic face, long braids that looked like two armfuls of fresh hay, and blue eyes you could drown in.",
		
	["Она широко улыбнулась."] = "She smiled broadly.",
	["Странно, но передо мной как будто совершенно обычная девочка."] = "Strange, it looked as if I had just a normal girl in front of me.",
	
	["Эх, не стоило вообще сюда возвращаться – лучше уж в леса, в поля..."] = "Bah, I shouldn't have returned here, the woods and fields seemed better...",

	["Но что делать дальше – пытаться общаться с ней как с человеком, бежать или ещё что-то?"] = "But what shall I do next – try to speak with her as if she was a human, or run away, or what?",
		
	["В голове невыносимо стучала кровь, разрывая её изнутри; ещё немного и бедную пионерку забрызгает неприглядным содержимым моей черепной коробки..."] = "The blood was pumping unbearably in my head, tearing it apart from the inside; a little bit more and the poor pioneer girl would be splattered with the gruesome contents of my skull...",
		
	["Девочка окинула меня взглядом."] = "The girl looked me over.",
	["По спине побежали мурашки, задрожали колени."] = "It sent shivers down my spine, and my knees started to tremble.",
	
	["Славно? Что тут может быть славного?"] = "Great? What's so great about that?",
	
	["Внезапно захотелось плюнуть на всё, забыть о стоящем сзади автобусе, вчерашней зиме и будущем лете, скинуть чесоточный свитер и просто поверить, что всё это на самом деле, что так и надо, что так и должно быть, что всё это, в конце концов, к лучшему..."] = "Suddenly a thought crossed my mind: to hell with it all. Forget about the bus behind me, the fact that it was winter yesterday and summer today. I wanted to rip off my itchy sweater and just accept that all this is actually happening. Everything is as it should be, all this is for the best...",
		
	["Она показала в сторону ворот, как будто я знал, что за ними."] = "She pointed at the gates, as if I knew what was behind them.",
		
	["Естественно, ничего я не понял."] = "Of course I didn't.",
	["Девочка помахала мне рукой и скрылась за воротами."] = "The girl waved her hand at me and disappeared through the gates.",
	["Казалось, что я для неё что-то такое...{w} нормальное."] = "It seemed as if to her, I was something...{w} ordinary.",
	["И весь этот цирк с автобусом, путешествиями во сне и наяву волнует только меня, а здесь всё так, как и должно быть."] = "And all this show with the bus and the travels while awake or asleep were troubling only to me, while everything here is just the way it is supposed to be.",
		
	["Вожатая, пионерская форма…"] = "Camp leader, pioneer uniform…",
	["Они что, здесь устроили историческую реконструкцию?"] = "What, are they doing a historical reenactment here?",
	["Надеюсь, на этой площади я не увижу Ленина на броневике?"] = "I hope I won't find Lenin standing atop an armored car in this square.",
	["Хотя сейчас меня и это не удивит…"] = "But even that would not surprise me right now...",
	["Постояв немного, я направился в лагерь."] = "After standing alone for a while, I headed into the camp.",
	
	["Я остолбенел от удивления."] = "I was frozen in astonishment.",
	["Пришлось приложить немало усилий, чтобы ответить:"] = "It took a monumental effort to answer:","Постояв немного, я направился в лагерь.",
	
	["Девочка выглядела совершенно нормально, даже была похожа на человека, но всё равно я не мог вымолвить ни слова."] = "The girl looked perfectly normal, just like an ordinary human, but I still could not utter a single word.",
		
	["Бежать уже поздно..."] = "It's too late to run...",
	["Быстро ответил я, уже было испугавшись, что ляпнул не то."] = "I replied quickly, beginning to worry that I might have blurted out something wrong.",
		
	-- Slavya --
	["Привет, ты, наверное, только что приехал?"] = "Hi, you must have just arrived?",
	["Что же, добро пожаловать!"] = "All right then, welcome!",
	["Что смешного?"] = "What's so funny about that?",
	["Ну и славно."] = "Great then.",
	["Тебе к вожатой надо, она всё расскажет!"] = "You should go to our camp leader, she'll tell you everything!",
	["Смотри.{w} Сейчас идёшь прямо-прямо, доходишь до площади, затем налево, дальше будут домики."] = "Look.{w} You go straight ahead to the square, then turn left. You'll see several small cabins.",
	["Ну спросишь у кого-нибудь, где домик Ольги Дмитриевны!"] = "Well, you can ask someone where Olga Dmitrievna's cabin is.",
	["Всё понял?"] = "Got it?",
	["А мне пора."] = "Well, I've got to go now.",
	["Я что-то не то сказала?"] = "Did I say something wrong?",
	["Что?"] = "What?",
	
	-- Semyon --
	["Н... ничего..."] = "N... nothing...",
	["Я… эээ…"] = "I… erm…",
	["Ну… да…"] = "Uhm... yeah...",
	["А, нет, я в смысле, что {i}только что приехал{/i}."] = "Oh, nothing, I mean I've just arrived.",
	["А ты, наверное, знаешь…"] = "Would you happen to know...?",
	
	["Семён"] = "Semyon",
	["Пионерка"] = "Pioneer girl",
	["Размышления"] = "Thoughts",
	
	-- GUI --
	["Не отвечать"] = "Keep silence",
	["Ответить"] = "Answer to her"
}