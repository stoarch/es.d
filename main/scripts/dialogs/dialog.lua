local M = {}

local HEADER = "header"
local H = HEADER
local DIALOG = "dialog"
local D = DIALOG
local ACTION = "action"
local A = ACTION

local function internal_step_dialog(self)
	self.position = self.position + 1
end

function M:step_dialog()
	internal_step_dialog(self)
end

function M:add_line( header, dialog, action )
	local count = #self.dialog_lines
	local line = {[HEADER]=header, [DIALOG]=dialog, [ACTION]=action}
	self.dialog_lines[ count + 1 ] = line
end

function M:start_line( header, dialog, action )
	self.dialog_lines[ 1 ] = {[HEADER]=header, [DIALOG]=dialog, [ACTION]=action}
end

function M:at_end()
	return self.position < #self.dialog_lines
end

function M.new(aname)
	local instance = {
		name = aname,
		position = 0,
		dialog_lines = {}
	}
	
	return setmetatable(instance, {__index = M})
end

return M
