-- SLAVYA MANIPULATION ROUTINES 
--
-- TODO: Move to module
local ANIMATED = true

SlavyaManager = {}

function SlavyaManager:show_slavya_far_at_center()
	self.slavya_actor_obj:show_far_at_center()
end

function SlavyaManager:show_slavya_at_center()
	self.slavya_actor_obj:show_at_center(self.outfit)
end

function SlavyaManager:show()
	self.slavya_actor_obj:show()
end

function SlavyaManager:hide_slavya()
	print('SlavyaManager:hide_slavya')
	self.slavya_actor_obj:hide()	
end

function SlavyaManager:hide_slavya_animated()
	self.slavya_actor_obj:hide_animated()
end

function SlavyaManager:hide_slavya_animated_to_left()
	self.slavya_actor_obj:hide_animated_to_left()
end

function SlavyaManager:show_slavya_animated_from_left()
	self.slavya_actor_obj:show_animated_from_left()
end

function SlavyaManager:highlight_slavya()
	self.slavya_actor_obj:highlight()
end

function SlavyaManager:dim_slavya()
	self.slavya_actor_obj:dim()
end

function SlavyaManager:zoom_in_slavya_animated()
	self.slavya_actor_obj:zoom_in_animated()
end

function SlavyaManager:zoom_out_slavya_animated()
	self.slavya_actor_obj:zoom_out_animated()
end

function SlavyaManager:make_slavya_at( x, y, scale )
	return self.slavya_actor_obj:make_at( x, y, scale )
end

function SlavyaManager:show_slavya_smile(animated)
	print("SlavyaManager:show_slavya_smile: outfit ".. tostring(self.outfit))
	self.slavya_actor_obj:show_smile(self.outfit, animated)
end

function SlavyaManager:show_slavya_smile2()
	self.slavya_actor_obj:show_smile2(self.outfit)
end

function SlavyaManager:show_slavya_normal()
	self.slavya_actor_obj:show_normal()
end

function SlavyaManager:show_slavya_surprise()
	self.slavya_actor_obj:show_surprise()
end

function SlavyaManager:show_slavya_icon()
	self.slavya_actor_obj:show_icon()
end

function SlavyaManager:hide_slavya_icon()
	self.slavya_actor_obj:hide_icon()
end

function SlavyaManager:set_slavya_text_color()
	print('sm:set_slavya_text_color')
	gui.set_color( self.text_node, YELLOW_COLOR )	 
end

function SlavyaManager:clear_background()
	if( self.background ~= nil )then
		msg.post( self.background, "delete" )
	end
end

function SlavyaManager:unload_background()
	if(self.background_proxy_url == nil)then
		print("Unable to unload nil background")
		return
	end

	msg.post(self.background_proxy_url, "disable")
	msg.post(self.background_proxy_url, "final")
	msg.post(self.background_proxy_url, "unload")
end

function SlavyaManager:show_slavya_at_gates_bg()
	self:unload_background()
	self.background_proxy_url = "/bg_proxies#gates_bg_proxy"
	msg.post(self.background_proxy_url, "load")
end

function SlavyaManager:receive_background(sender)
	self.background = "bg:/background"
	print(self.background)	
end

local function bind( t, k )
	return function(...)
		print(k) 
		return t[k](t, ...) 
	end
end

--- Slavya relations dialog ---

function SlavyaManager:show_relations_with_slavya_animated()
	gui.set_position(self.slavya_rel_icon, vmath.vector3(2000, 500, 1))
	self:show_slavya_hearts_in_gui()			
	gui.animate(
		self.slavya_rel_icon, 
		"position.x", 
		1100, 
		gui.EASING_OUTEXPO, 
		1, 
		0, 
		make_sequence(
			bind(self, 'show_slavya_new_heart'), 
			bind(self, 'hide_slavya_relation_gui')), 
		gui.PLAYBACK_ONCE_FORWARD)	
end

function SlavyaManager:show_slavya_hearts_in_gui()
	local hearts = {}
	for i = 1, 10 do
		hearts[i] = gui.get_node("slavya_relations/heart" .. i .. "_icon")

		if( i < self.slavya_relation_to_semyon )then
			gui.play_flipbook(hearts[i], "Heart-icon" )		
		else
			gui.play_flipbook(hearts[i], "black_heart_icon_128x128") 
		end
	end
end

function SlavyaManager:hide_slavya_relation_gui()
	gui.animate(self.slavya_rel_icon, "position.x", 2000, gui.EASING_INEXPO, 1, 3, nil, gui.PLAYBACK_ONCE_FORWARD)	
end

function SlavyaManager:show_slavya_new_heart()
	local last_heart = gui.get_node("slavya_relations/heart" .. self.slavya_relation_to_semyon .. "_icon")
	gui.play_flipbook(last_heart, "Heart-icon")
	gui.set_scale(last_heart, vmath.vector3(0.1,0.1,1))
	gui.animate(last_heart, "scale", vmath.vector3(0.7,0.7,1.0), gui.EASING_OUTELASTIC, .5, 0, nil, gui.PLAYBACK_ONCE_FORWARD) 
end

function SlavyaManager:hide_icons()
	self.slavya_actor_obj:hide_icon()
end

function SlavyaManager:set_outfit_swim()
	self.outfit = "swim"
end

function SlavyaManager:set_outfit_pioneer()
	self.outfit = "pioneer"
end

local SlavyaActorModule = require("main.scripts.dialogs.actors.slavya_actor")

function SlavyaManager:new(aslavya_icon, aslavya_rel_icon)
	local instance = {
		slavya_actor_obj = nil,
		slavya_relation_to_semyon = 0,
		slavya_rel_icon = aslavya_rel_icon,
		background = nil,
		text_node = nil,
		slavya_icon = aslavya_icon,
		outfit = "pioneer",
		background_proxy_url = nil
	}
		
	CENTER_X = 600
	CENTER_Y = 400
	
	instance.slavya_actor_obj = SlavyaActorModule.new(instance.slavya_icon, CENTER_X, CENTER_Y)
	instance.slavya_actor_obj.manager = self
	instance.slavya_actor_obj:hide_icon()

	instance.text_node = gui.get_node( "dialogText" )

	setmetatable(instance, {__index = SlavyaManager})
		
	return instance
end

