local M = {}

function M:show_far_at_center()
	self.slavya_actor = self:show_far_at( self.slavya_actor, self.CENTER_X, self.CENTER_Y, self.ANIMATED )
end

function M:show_at_center(outfit)
	self.slavya_actor = self:show_at( self.slavya_actor, self.CENTER_X, self.CENTER_Y, 1, self.ANIMATED, outfit )
end

function M:show_far_at( actor, x, y, animated )	
	return self:show_at( actor,  x, y - 200, 0.5, animated )
end

function M:show_at( actor, x, y, ascale, animated, outfit )

	local new_scale = 1
	if( ascale ~= nil )then
		new_scale = ascale 
	end 

	local new_outfit = "pioneer"
	if( outfit ~= nil )then
		new_outfit = outfit
	end

	if( actor == nil )then
		actor = self:make_at( x, y, new_scale )
		msg.post( script_url( actor ), "set_emotion", {emotion = "normal", outfit = new_outfit })				
	end

	local pos = vmath.vector3( x, y, 0.5 )
	local url = script_url( actor )
	print( url )

	if( animated ~= nil and animated )then
		msg.post( url, "show_animated" )
	else
		msg.post( url, "show" )
	end

	msg.post( url, "set_position", {position = pos})
	msg.post( url, "set_new_scale", {scale = new_scale})

	return actor
end

function M:slavya_url()	
	print('SlavyaActor: show :' .. script_url(self.slavya_actor))
	pprint(self.slavya_actor)
	return script_url(self.slavya_actor)
end

function M:show()
	msg.post(self:slavya_url(), "show",{})
end

function M:hide()	
	msg.post(self:slavya_url(), "hide", {})
end

function M:hide_animated()
	msg.post(self:slavya_url(), "hide_animated", {})
end

function M:hide_animated_to_left()
	msg.post(self:slavya_url(), "hide_animated_left", {})
end

function M:show_animated_from_left()
	msg.post(self:slavya_url(), "show_animated_from_left", {})
end

function M:highlight()
	if( self.slavya_actor == nil )then 
		return nil
	end

	local url = self:slavya_url()
	print(url)

	msg.post( url, "highlight" )
end

function M:dim()
	if( self.slavya_actor == nil )then 
		return nil 
	end
	
	local url = self:slavya_url()
	
	msg.post( url, "dim" )
end

function M:zoom_in_animated()
	msg.post(self:slavya_url(), "zoom", {timeout=1.0, scale=1.0})
end

function M:zoom_out_animated()
	msg.post(self:slavya_url(), "zoom", {timeout=1.0, scale=0.5})
end

function M:make_at( x, y, scale )
	local pos = vmath.vector3( x, y, 0.5 )
	local component = "/actor_factories#slavya_factory"
	local lscale = 1
	if( scale ~= nil )then 
		lscale = scale
	end
		
	inst = factory.create( component, pos, nil, {}, lscale )
	print('SlavyaActor:: make_at ' .. x .. ', ' .. y .. ', ' .. scale )
	pprint(inst)
	
	self.slavya_actor = inst
	
	return inst
end

function M:show_smile(outfit, animated)
	print("SlavyaActor:show_smile "..outfit)
	print(self:slavya_url())

	if( outfit == nil )then 
		outfit = "pioneer"
	end
	if( animated == nil )then
		animated = true
	end

	if( animated )then
		msg.post(self:slavya_url(), "set_emotion_animated", {emotion = "smile", outfit = outfit })
	else
		msg.post(self:slavya_url(), "set_emotion", {emotion = "smile", outfit = outfit })
	end
end

function M:show_smile2(outfit)
	new_outfit = "pioneer"
	if outfit ~= nil then
		new_outfit = outfit
	end
	
	msg.post(self:slavya_url(), "set_emotion_animated", {emotion = "smile2", outfit = new_outfit})
end

function M:show_normal()
	msg.post(self:slavya_url(), "set_emotion_animated", {emotion = "normal", outfit = "pioneer"})
end

function M:show_surprise()
	msg.post(self:slavya_url(), "set_emotion_animated", {emotion = "surprise", outfit = "pioneer"})
end

function M:show_icon()
	gui.set_enabled( self.slavya_icon, true )	
end

function M:hide_icon()
	gui.set_enabled( self.slavya_icon, false )
end

function M.new(slavya_icon_parm, CENTER_XP, CENTER_YP)
	local instance = {
		slavya_actor = nil,
		slavya_icon = slavya_icon_parm,
		CENTER_X = CENTER_XP,
		CENTER_Y = CENTER_YP,
		ANIMATED = true,
		manager = nil
	}
	
	return setmetatable(instance, {__index = M})
end

return M

